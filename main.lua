import = require("src.util.import")

Screens = require("src.util.screens")
local input = require("src.input")
game = require("src.game")

function love.load()
  -- Load input
  input.setup()

  -- Load font
  local font = love.graphics.newFont("assets/font/PixelEmulator-xq08.ttf", 20)
  love.graphics.setFont(font)

  -- Starting state
  Screens:push(game)
end

function love.update(dt)
  Screens:update(dt)
end

function love.draw()
  Screens:draw()
end

function love.resize(...)
  Screens:resize(...)
end

function love.quit()
  -- Save input
  input.save()
end

function love.wheelmoved(x, y)
  input:wheelmoved(x, y)
  Screens:wheelmoved(x, y)
end

for _,name in next,{"mousepressed", "mousereleased", "mousemoved", "textinput", "keypressed", "keyreleased"} do
  love[name] = function(...)
    Screens[name](Screens, ...)
  end
end

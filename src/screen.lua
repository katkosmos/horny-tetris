local screen = {}

-- Calculates usable screen area for game and borders
function screen:getUsableScreen(x, y, w, h)
  local aspect = self.aspect

  if w == h then
    -- Square aspect ratio
    local main = {
      x = x,
      y = y,
      w = aspect*h,
      h = h,
    }

    return {
      main = main,
      borders = {
        {
          x = x + main.w,
          y = y,
          w = w - main.w,
          h = h,
        },
      },
    }
  elseif w > h and (w - aspect*h)/2 >= 100 then
    -- Computer aspect ratio
    local main = {
      x = (w - aspect*h)/2,
      y = y,
      w = aspect*h,
      h = h,
    }

    return {
      main = main,
      borders = {
        {
          x = x,
          y = y,
          w = main.x,
          h = h,
        },
        {
          x = main.x + main.w,
          y = y,
          w = main.x,
          h = h,
        },
      },
    }
  elseif w > h then
    -- Computer aspect ratio
    local main = {
      x = 0,
      y = y,
      w = aspect*h,
      h = h,
    }

    return {
      main = main,
      borders = {
        {
          x = main.w,
          y = y,
          w = w - main.w,
          h = h,
        },
      },
    }
  else
    -- Phone aspect ratio
    local main = {
      x = x,
      y = y,
      w = w,
      h = (1/aspect)*w,
    }

    return {
      main = main,
      borders = {
        {
          x = x,
          y = y + main.h,
          w = w,
          h = h - main.h,
        },
      },
    }
  end
end

-- Initializes screen
function screen:setup(aspect)
  self.aspect = aspect
  local x, y, w, h = love.window.getSafeArea()
  self.areas = self:getUsableScreen(x, y, w, h)
  self.canvas = love.graphics.newCanvas(self.areas.main.w, self.areas.main.h)
end

-- Call before drawing main game screen
function screen:start(border)
  assert(not self.active_canvas, "did not finish")
  self.active_canvas = true
  
  if not border then
    love.graphics.setCanvas(self.canvas)
    love.graphics.clear(0.2, 0.2, 0.2, 1)
    love.graphics.setBlendMode("alpha")
  elseif self.borders[border] then
    -- @TODO
  else
    self.active_canvas = false
  end
end

-- Call after drawing main game screen
function screen:finish()
  love.graphics.setCanvas()
  love.graphics.setBlendMode("alpha", "premultiplied")
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.draw(self.canvas, self.areas.main.x, self.areas.main.y)
  self.active_canvas = false
end

-- Converts screen coordinates to game coords
function screen:toGame(x, y)

end

-- Resize the screen
function screen:resize()
  local x, y, w, h = love.window.getSafeArea()
  self.areas = self:getUsableScreen(x, y, w, h)
  self.canvas = love.graphics.newCanvas(self.areas.main.w, self.areas.main.h)
end

return screen

local import = require("src.util.import")
local clone = require("src.util.clone")
local bitser = require("lib.bitser")

local NON_BATON = {"mouse:drag", "mouse:wheel-", "mouse:wheel+", "touch:drag", "touch:swipedown", "touch:swipeleft", "touch:swipeup", "touch:swiperight", "touch:tapleft", "touch:tapright", "touch:tapup", "touch:tapdown", "touch:tap"}
local DEFAULT_CONTROLS_CONFIG = {controls={}, pairs={move={"left","right","up","down"}},nonbaton={}}
-- Import default control schemes, excluding mouse
for schemeName,scheme in next,import("schemes") do
  if schemeName ~= "mouse" then
    for action,triggers in next,scheme do
      DEFAULT_CONTROLS_CONFIG.controls[action] = DEFAULT_CONTROLS_CONFIG.controls[action] or {}
      DEFAULT_CONTROLS_CONFIG.nonbaton[action] = DEFAULT_CONTROLS_CONFIG.nonbaton[action] or {}
      local actions = DEFAULT_CONTROLS_CONFIG.controls[action]
      local nonbaton = DEFAULT_CONTROLS_CONFIG.nonbaton[action]
      for i,v in ipairs(triggers) do
        local found = false
        for _,vv in ipairs(NON_BATON) do
          if v == vv then
            found = true
            break
          end
        end

        if not found then
          table.insert(actions, v)
        else
          table.insert(nonbaton, v)
        end
      end
        
      --[[
      if #actions > 0 then
        print(schemeName.."."..action.." = {'"..table.concat(actions, "', '").."'}")
      else
        print(schemeName.."."..action.." = {}")
      end
      ]]
    end
  end
end

local config = {}

function config.load()
  local cfg = clone(DEFAULT_CONTROLS_CONFIG)

  if love.filesystem.getInfo("input.cfg") ~= nil then
    local contents, size = love.filesystem.read("string", "input.cfg")
    if contents ~= nil then
      cfg = bitser.loads(contents)
    end
  end

  return cfg
end

function config.save(cfg)
  assert(type(cfg) == "table", "invalid config obj")
  local configStr = bitser.dumps(cfg)
  local success, message = love.filesystem.write("input.cfg", configStr)
end

return config

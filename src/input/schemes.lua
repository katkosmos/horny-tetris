return {
  keyboard = {
    up = {"key:up", "key:w"};
    down = {"key:down", "key:s"};
    left = {"key:left", "key:a"};
    right = {"key:right", "key:d"};
    rotleft = {"key:q"};
    rotright = {"key:e"};
    drop = {"key:return", "key:space", "key:x"};
    cancel = {"key:escape", "key:z"};
    confirm = {"key:return", "key:space", "key:x"};
  },
  mouse = {
    movehoriz = {"mouse:drag"};
    rotleft = {"mouse:1"};
    rotright = {"mouse:2"};
    confirm = {"mouse:1"};
    drop = {"mouse:3"};
  },
  touch = {
    movehoriz = {"touch:drag"};
    drop = {"touch:swipedown"};
    rotleft = {"touch:tapleft"};
    rotright = {"touch:tapright"};
  },
  gamepad = {
    up = {"axis:lefty-", "button:dpup"};
    down = {"axis:lefty+", "button:dpdown"};
    left = {"axis:leftx-", "button:dpleft"};
    right = {"axis:leftx+", "button:dpright"};
    rotleft = {"button:leftshoulder", "axis:lefttrigger+"};
    rotright = {"button:rightshoulder", "axis:righttrigger+"};
    drop = {"button:a"};
    cancel = {"button:b"};
    confirm = {"button:a"};
  },
}

local import = require("src.util.import")
local controls_config = import("config")
local baton

local input = {
  baton = nil,
  pendingSwipes = {},
  pendingTaps = {},
  pendingTouchDrag = false,
  pendingMouseDrag = false,
}
local timer
local DELAY = 0.2
local SWIPE_THRESHOLD = 30

local function processNonBaton(control)
    for _,name in ipairs(input.baton.config.nonbaton[control]) do
      if name == "touch:tap" then
        -- Generic tap
        if #input.pendingTaps > 0 then
          table.remove(input.pendingTaps)
          return true
        end
      elseif name:sub(1,9) == "touch:tap" then
        -- Tap in a quadrant
        local dir = name:sub(10)
        for pi,tdir in ipairs(input.pendingTaps) do
          if tdir == dir then
            table.remove(input.pendingTaps, pi)
            return true
          end
        end
      elseif name == "touch:swipe" then
        -- Generic swipe
        if #input.pendingSwipes > 0 then
          table.remove(input.pendingSwipes)
          return true
        end
      elseif name:sub(1,11) == "touch:swipe" then
        -- Swipe in a direction
        local dir = name:sub(12)
        for pi,sdir in ipairs(input.pendingSwipes) do
          if sdir == dir then
            table.remove(input.pendingSwipes, pi)
            return true
          end
        end
      elseif name == "mouse:drag" then
        local b = input.pendingMouseDrag
        input.pendingMouseDrag = false
        return b
      elseif name == "touch:drag" then
        local b = input.pendingTouchDrag
        input.pendingTouchDrag = false
        return b
      elseif name == "mouse:wheel-" then
        local mw = input.mouseWheelDy
        input.mouseWheelDy = 0
        return mw < 0
      elseif name == "mouse:wheel+" then
        local mw = input.mouseWheelDy
        input.mouseWheelDy = 0
        return mw > 0
      else
        print("unsupported action: "..name)
      end
    end
end

function input.setup()
  baton = require("lib.baton")
  timer = 0

  input.baton = baton.new(controls_config.load())
end

function input.save()
  controls_config.save(input.baton.config)
end

function input.update(dt)
  input.baton:update()
  timer = math.max(timer - dt, 0)
end

function input:pressed(control)
  return input.baton:down(control) or processNonBaton(control)
end

function input.left()
  if timer == 0 then
    if input:pressed("left") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.right()
  if timer == 0 then
    if input:pressed("right") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.rotleft()
  if timer == 0 then
    if input:pressed("rotleft") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.rotright()
  if timer == 0 then
    if input:pressed("rotright") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.down()
  if timer == 0 then
    if input:pressed("down") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.up()
  if timer == 0 then
    if input:pressed("up") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.drop()
  if timer == 0 then
    if input:pressed("drop") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.select()
  if timer == 0 then
    if input:pressed("confirm") then
      timer = DELAY
      return true
    end
  end

  return false
end

function input.cancel()
  if timer == 0 then
    if input:pressed("cancel") then
      timer = DELAY
      return true
    end
  end

  return false
end

local isSwiping, beginSwipeXY
function input:touchpressed(id, x, y, dx, dy, pressure)
  if not isSwiping then
    isSwiping = true
    beginSwipeXY = {x,y}
  end
end

function input:touchmoved(id, x, y, dx, dy, pressure)
  input.pendingTouchDrag = true
end

function input:touchreleased(id, x, y, dx, dy, pressure)
  if isSwiping then
    dx = x - beginSwipeXY[1]
    dy = y - beginSwipeXY[2]

    -- If above the swiping threshold
    if math.sqrt(dx^2 + dy^2) >= SWIPE_THRESHOLD then
      if math.abs(dx) > math.abs(dy) then
        -- Horizontal
        if beginSwipeXY[1] < x then
          -- Swipe right
          table.insert(input.pendingSwipes, "right")
        else
          -- Swipe left
          table.insert(input.pendingSwipes, "left")
        end
      else
        -- Vertical
        if beginSwipeXY[2] < y then
          -- Swipe down
          table.insert(input.pendingSwipes, "down")
        else
          -- Swipe up
          table.insert(input.pendingSwipes, "up")
        end
      end
    end
  else
    -- Otherwise, it's a tap
    local w, h = love.graphics.getDimensions()
    local dx, dy = x - w*0.5, y - h*0.5

    -- Left/right
    if math.abs(dx) >= math.abs(dy) then
      if dx > 0 then
        table.insert(input.pendingTaps, "right")
      else
        table.insert(input.pendingTaps, "left")
      end
    else
      -- Up/down
      if dy > 0 then
        table.insert(input.pendingTaps, "down")
      else
        table.insert(input.pendingTaps, "up")
      end
    end
  end
end

function input:wheelmoved(x, y)
  self.mouseWheelDy = y
end

return input

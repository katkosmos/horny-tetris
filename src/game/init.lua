local import = require("src.util.import")
local Screens = require("src.util.screens")
local Music = require("src.music")
local Cum = require("src.cum")

local game = {
  WIDTH = 10,
  HEIGHT = 20,
}

local Pieces, grid, currentPiece, nextPiece
local nextPieceCanvas
local Input, Screen

local DROP_DELAY = 1
local dropTimer = DROP_DELAY

local score, combo
local lost

local function getBlock(x, y)
  return grid[y] ~= nil and grid[y][x] ~= nil
end

local function newPiece()
  currentPiece = nextPiece
  nextPiece = { x = 2, y = 0, dir = 0, name = Pieces.random() }
  
  if not currentPiece then
    currentPiece = { x = 2, y = 0, dir = 0, name = Pieces.random() }
  end
  
  if Pieces.occupied(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, game.WIDTH, game.HEIGHT, getBlock) then
    currentPiece = nil
    nextPiece = nil
    nextPieceCanvas = nil
    game:lose()
    return nil
  end

  nextPieceCanvas = love.image.newImageData(5,5)
  local c = Pieces.getColor(nextPiece.name)
  Pieces.eachBlock(nextPiece.name, 0, 0, nextPiece.dir, function(x, y)
    nextPieceCanvas:setPixel(x, y, c[1], c[2], c[3])
  end)
  nextPieceCanvas = love.graphics.newImage(nextPieceCanvas)
  nextPieceCanvas:setFilter("nearest")

  return currentPiece
end

function game:setup()
  math.randomseed(os.time())
  Music:render()

  Pieces = import("pieces5")
  Input = require("src.input")

  Screen = require("src.screen")
  Screen:setup(self.WIDTH/self.HEIGHT)

  Cum:init()

  score, combo = 0, -1
  lost = false
  
  grid = {}
  for y=1,self.HEIGHT do
    grid[y] = {}
  end

  newPiece()
end

function game:update(dt)
  -- @TODO: use to spawn cum on line clears
  -- Cum:add(math.random()*self.WIDTH, math.random()*self.HEIGHT)

  Music:update(dt)
  Cum:update(dt)
  Input.update(dt)

  local rotation = 0
  local dx, dy = 0, 0
  
  dropTimer = math.max(dropTimer - dt, 0)
  if dropTimer == 0 then
    dropTimer = DROP_DELAY
    dy = 1
  end

  if Input.rotright() then
    rotation = 1
  elseif Input.rotleft() then
    rotation = -1
  elseif Input.left() then
    dx = -1
  elseif Input.right() then
    dx = 1
  elseif Input.down() and dy == 0 then
    dy = dy + 1
  end

  if not currentPiece then
    return
  end

  if rotation ~= 0 then
    local origDir = currentPiece.dir
    currentPiece.dir = currentPiece.dir + rotation
    if currentPiece.dir > 3 then
      currentPiece.dir = 0
    elseif currentPiece.dir < 0 then
      currentPiece.dir = 3
    end

    if Pieces.occupied(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
      -- check adjacent spaces to see if we can move into one of them
      if Pieces.unoccupied(currentPiece.name, currentPiece.x - 1, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
        dx = -1
      elseif Pieces.unoccupied(currentPiece.name, currentPiece.x + 1, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
        dx = 1
      elseif Pieces.unoccupied(currentPiece.name, currentPiece.x + 2, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
        dx = 2
      elseif Pieces.unoccupied(currentPiece.name, currentPiece.x - 2, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
        dx = -2
      else
        currentPiece.dir = origDir
      end
    end
  end
  
  -- Handle horizontal movement after rotation in case of rotation causing a move
  if dx ~= 0 then
    local origX = currentPiece.x
    currentPiece.x = origX + dx

    if Pieces.occupied(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
      currentPiece.x = origX
    end
  end
  
  if dy ~= 0 then
    local origY = currentPiece.y
    currentPiece.y = origY + dy

    if Pieces.occupied(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, self.WIDTH, self.HEIGHT, getBlock) then
      currentPiece.y = origY

      if currentPiece.name == 'I' or currentPiece.name == "O" and (currentPiece.dir == 1 or currentPiece.dir == 3) then
        local distance = 0
        
        Pieces.eachBlock(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, function(x, y)
          if (getBlock(x - 1, y) or x <= 0) and (getBlock(x + 1, y) or x >= self.WIDTH - 1 ) then
            distance = distance + 1
          end
        end)
        
        if distance > 0 then
          print("effect "..distance)
        end
      end
      
      -- Write piece to grid
      local c = Pieces.getColor(currentPiece.name)
      Pieces.eachBlock(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, function(x, y)
        grid[y] = grid[y] or {}
        grid[y][x] = c
        -- Add a cum wall for each piece
        Cum:addWall(x, y, 1, 1)
      end)
      
      -- Simplify walls
      Cum:simplifyWalls()
    
      -- Check for line clears
      local clearedLinesCount, level = self:handleLineClears()
      if clearedLinesCount > 0 then
        combo = combo + 1
      else
        if combo > 0 then
          -- Add combo score
          score = score + 50*combo*level
        end

        combo = -1
      end

      -- Get a new piece
      newPiece()

      -- Print score
      print(score)
    end
  end
end

function game:draw()
  Screen:start()

  local canvas = love.graphics.getCanvas()
  local w, h = canvas:getDimensions()
  local sw, sh = w / self.WIDTH, h / self.HEIGHT

  for y=1,self.HEIGHT do
    y = y - 1
    if grid[y] then
      for x=1,self.WIDTH do
        x = x - 1
        local c = grid[y][x]
        if c then
          love.graphics.setColor(c[1], c[2], c[3], 1.0)
          love.graphics.rectangle("fill", x*sw, y*sh, sw, sh)
        end
      end
    end
  end
  
  if currentPiece then
    local c = Pieces.getColor(currentPiece.name)
    love.graphics.setColor(c[1], c[2], c[3], 1.0)

    Pieces.eachBlock(currentPiece.name, currentPiece.x, currentPiece.y, currentPiece.dir, function(x, y)
      love.graphics.rectangle("fill", x*sw, y*sh, sw, sh)
    end)
  end
  
  -- Draw cum effect
  Cum:draw(sw, sh)

  Screen:finish()
  
  if nextPieceCanvas then
    --Screen:start(1)

    love.graphics.setBlendMode("alpha")
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(nextPieceCanvas, 0, 32, 0, 32, 32)


    love.graphics.print(tostring(score), 0, 0)

    --Screen:finish()
  end
end

function game:lose()
  lost = true
  Screens:push(import("lose"))
end

function game:handleLineClears()
  local clearedLines = {}
  local level = -1

  -- Check for cleared lines
  for y=1,self.HEIGHT do
    y = y - 1

    if grid[y] then
      local cleared = true
      
      for x=1,self.WIDTH do
        x = x - 1

        cleared = cleared and grid[y][x] ~= nil
        if not cleared then
          break
        end
      end

      if cleared then
        grid[y] = nil
        -- Remove cum walls from cleared line
        Cum:removeWallsInside(0,y,self.WIDTH,1)

        table.insert(clearedLines, y+1)
        level = y
      end
    end
  end

  if #clearedLines == 0 then
    return 0, level
  end

  -- Sort the cleared lines so the lines above are dropped properly
  table.sort(clearedLines)
  -- Drop the lines above cleared lines
  for _,y in ipairs(clearedLines) do
    for y=y,1,-1 do
      y = y - 1
      grid[y] = grid[y-1]

      -- Move cum walls down
      for i,w in ipairs((Cum or {}).walls or {}) do
        if w.p.y == y-1 then
          w.p.y = y
        end
      end
    end
  end

  -- Scoring cleared lines
  local clearedCounts = {} -- (count, last_line)[]
  local last, count = -1, 1

  for _,line in ipairs(clearedLines) do
    if last == line - 1 then
      count = count + 1
    else
      if count ~= 0 then
        local multiplier
        if count == 1 then
          multiplier = 100
        elseif count == 2 then
          multiplier = 300
        elseif count == 3 then
          multiplier = 500
        elseif count == 4 then
          multiplier = 800
        elseif count == 5 then
          multiplier = 1200
        end

        score = score + multiplier*line
      end

      count = 1
    end

    last = line
  end

  -- Remove cleared lines that may result from newly cleared lines,
  -- return total count of cleared lines
  local n = #clearedLines
  repeat
    local nn, maxlevel = self:handleLineClears()
    
    if maxlevel then
      level = math.max(level, maxlevel)
    end

    n = n + 1
  until nn == 0

  return n, level
end

function game:resize(...)
  Screen:resize(...)
end

return game

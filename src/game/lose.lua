local helium = require("lib.helium")
local scene

local lose = {}

function lose:load()
  scene = helium.scene.new(true)
  scene:activate()
end

function lose:update(dt)
  scene:update(dt)
end

function lose:draw()
  scene:draw()
end

function lose:resize(...)
  scene:resize(...)
end

return lose

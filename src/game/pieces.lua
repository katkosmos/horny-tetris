local bit = require("bit")

local pieces = {}

local names = {'Z', 'T', 'S', 'O', 'L', 'J', 'I'}

local colors = {
  {1, 0, 0}, -- Z
  {160/0xff, 32/0xff, 240/0xff}, -- T
  {0, 1, 0}, -- S
  {1, 1, 0}, -- O
  {1, 0xA5 / 0xff, 0}, -- L
  {0, 0, 1}, -- J
  {0, 1, 1}, -- I
}

local shapes = {
  { 0x0c60, 0x4c80, 0xc600, 0x2640 }, -- Z
  { 0x0e40, 0x4c40, 0x4e00, 0x4640 }, -- T
  { 0x06c0, 0x8c40, 0x6c00, 0x4620 }, -- S
  { 0xcc00, 0xcc00, 0xcc00, 0xcc00 }, -- O
  { 0x4460, 0x0e80, 0xc440, 0x2e00 }, -- L
  { 0x44c0, 0x8e00, 0x6440, 0x0e20 }, -- J
  { 0x0f00, 0x2222, 0x00f0, 0x4444 }, -- I
}

local function getPiece(name)
  if type(name) == "number" then
    return (not not names[name]) and name or nil
  elseif type(name) == "string" then
    local index = nil

    for i,n in next,names do
      if n == name then
        index = i
        break
      end
    end

    return index
  end
end

function pieces.eachBlock(name, x, y, dir, fn)
  local b, result, row, col = 0, 0, 0, 0
  local piece = assert(getPiece(name), "invalid name: "..tostring(name))
  local blocks = shapes[piece][dir + 1]
  assert(blocks, "invalid name or direction")

  local b = 0x8000
  while b > 0 do
    -- Call callback function
    if bit.band(blocks, b) ~= 0 then
      local res = fn(x + col, y + row)
      if res ~= nil and res == false then
        break
      end
    end
    
    -- Increment row and col
    col = col + 1
    if col == 4 then
      col = 0
      row = row + 1
    end
    
    -- Shift bit
    b = b / 2
  end
end

function pieces.occupied(name, x, y, dir, maxX, maxY, getBlockFn)
  local result = false

  pieces.eachBlock(name, x, y, dir, function(x, y)
    if x < 0 or x >= maxX or y < 0 or y >= maxY or getBlockFn(x, y) then
      result = true
      return false
    end
  end)

  return result
end

-- Inverse of pieces.occupied
function pieces.unoccupied(name, x, y, dir, maxX, maxY, getBlockFn)
  return not pieces.occupied(name, x, y, dir, maxX, maxY, getBlockFn)
end

do
  local bag = {}
  local function resetBag()
    for _,name in next,names do
      bag[#bag + 1] = name
      bag[#bag + 1] = name
      bag[#bag + 1] = name
      bag[#bag + 1] = name
    end
  end

  resetBag()

  function pieces.random()
    if #bag == 0 then
      resetBag()
    end

    local i = math.floor(math.random()*(#bag - 1)) + 1
    local name = bag[i]
    table.remove(bag, i)
    return name
  end
end

function pieces.getColor(name)
  local piece = assert(getPiece(name), "invalid name")
  return colors[piece]
end

function pieces.draw(name, dir, x, y, w, h)
  local piece = getPiece(name)
  local c = colors[piece]
  
  local gsx, gsy = w / 4, h / 4
  
  love.graphics.setColor(c[1], c[2], c[3], 1)
  pieces.eachBlock(name, 0, 0, dir, function(gx, gy)
    love.graphics.rectangle("fill", x, y, gsx*gx, gsy*gy)
  end)
end

return pieces

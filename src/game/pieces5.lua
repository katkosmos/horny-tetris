local bit = require("bit")
local binary = require("src.util.binary")

local pieces = {}

local names = "FILNPTUVWXYZ-ROQSPTUVWXYZ"; do
  local new_names = {}

  while string.len(names) ~= 0 do
    local len = string.len(names)

    for i=1,len do
      local name = string.sub(names, 1, 1)
      names = string.sub(names, 2)
      if name == '-' then
        break
      end

      if new_names[i] then
        table.insert(new_names[i], name)
      else
        new_names[i] = {name}
      end
    end
  end

  names = new_names
end

local colors = {
  {1, 0, 0}, -- Z
  {160/0xff, 32/0xff, 240/0xff}, -- T
  {0, 1, 0}, -- S
  {1, 1, 0}, -- O
  {1, 0xA5 / 0xff, 0}, -- L
  {0, 0, 1}, -- J
  {0, 1, 1}, -- I
  {1, 0, 0}, -- Z
  {160/0xff, 32/0xff, 240/0xff}, -- T
  {0, 1, 0}, -- S
  {1, 1, 0}, -- O
  {1, 0xA5 / 0xff, 0}, -- L
  {0, 0, 1}, -- J
  {0, 1, 1}, -- I
  {1, 0, 0}, -- Z
  {160/0xff, 32/0xff, 240/0xff}, -- T
  {0, 1, 0}, -- S
  {1, 1, 0}, -- O
  {1, 0xA5 / 0xff, 0}, -- L
  {0, 0, 1}, -- J
  {0, 1, 1}, -- I
  {1, 0, 0}, -- Z
  {160/0xff, 32/0xff, 240/0xff}, -- T
  {0, 1, 0}, -- S
  {1, 1, 0}, -- O
  {1, 0xA5 / 0xff, 0}, -- L
  {0, 0, 1}, -- J
  {0, 1, 1}, -- I
}

local shapes = {}; do
  -- FILNPTUVWXYZ
  local shape_bitmaps = {
    binary[[00000 00110 01100 00100 00000]], -- F / R
    binary[[00100 00100 00100 00100 00100]], -- I / O
    binary[[00000 00100 00100 00100 00110]], -- L / Q
    binary[[00000 00010 00110 00100 00100]], -- N / S
    binary[[00000 00110 00110 00100 00000]], -- P / P
    binary[[00000 01110 00100 00100 00000]], -- T / T
    binary[[00000 00000 01010 01110 00000]], -- U / U
    binary[[00000 01000 01000 01110 00000]], -- V / V
    binary[[00000 01000 01100 00110 00000]], -- W / W
    binary[[00000 00100 01110 00100 00000]], -- X / X
    binary[[00000 00100 01100 00100 00100]], -- Y / Y
    binary[[00000 01100 00100 00110 00000]], -- Z / Z
  }
  
  --[[--
  local binstr = ""
  for i=1,25 do
    if bit.band(shape_bitmaps[4], bit.lshift(1, i - 1)) ~= 0 then
      binstr = binstr..'1'
    else
      binstr = binstr..'0'
    end

    if i % 5 == 0 then
      binstr = binstr..' '
    end
  end

  print(binstr)
  --]]--

  for bi,bitmap in ipairs(shape_bitmaps) do
    local rotated_graphics = {bitmap}
    
    for i=1,3 do
      local bitmap = rotated_graphics[#rotated_graphics]
      local rot = 0

      local popCount = 0
      
      for bitIndex=0,24 do
        if bit.band(bitmap, bit.lshift(1, bitIndex)) ~= 0 then
          local x, y = (bitIndex % 5), math.floor(bitIndex / 5)
          local bitIndexPrime = x*5 - y + 4
          rot = bit.bor(rot, bit.lshift(1, bitIndexPrime))
          popCount = popCount + 1
        end
      end

      assert(popCount == 5, "Popcount for shape bitmap "..bi.." ("..names[bi][1].."/"..names[bi][2]..") is not 5: got "..popCount)

      table.insert(rotated_graphics, rot)
    end

    table.insert(shapes, rotated_graphics)
  end
end

local function getPiece(name)
  if type(name) == "number" then
    return (not not names[name]) and name or nil
  elseif type(name) == "string" then
    local index = nil
    
    local stop = false
    for i,n in ipairs(names) do
      for j,n in ipairs(n) do
        if n == name then
          index = i
          stop = true
          break
        end
      end

      if stop then
        break
      end
    end

    return index
  end
end

function pieces.eachBlock(name, x, y, dir, fn)
  local b, result, row, col = 0, 0, 0, 0
  local piece = assert(getPiece(name), "invalid name: "..tostring(name))
  local blocks = shapes[piece][dir + 1]
  assert(blocks, "invalid name or direction")

  local b = 2^24
  while b > 0 do
    -- Call callback function
    if bit.band(blocks, b) ~= 0 then
      local res = fn(x + col, y + row)
      if res ~= nil and res == false then
        break
      end
    end
    
    -- Increment row and col
    col = col + 1
    if col == 5 then
      col = 0
      row = row + 1
    end
    
    -- Shift bit
    b = b / 2
  end
end

function pieces.occupied(name, x, y, dir, maxX, maxY, getBlockFn)
  local result = false

  pieces.eachBlock(name, x, y, dir, function(x, y)
    if x < 0 or x >= maxX or y < 0 or y >= maxY or getBlockFn(x, y) then
      result = true
      return false
    end
  end)

  return result
end

-- Inverse of pieces.occupied
function pieces.unoccupied(name, x, y, dir, maxX, maxY, getBlockFn)
  return not pieces.occupied(name, x, y, dir, maxX, maxY, getBlockFn)
end

do
  local bag = {}
  local function resetBag()
    for _,name in ipairs(names) do
      -- Place 5 of each piece in bag
      table.insert(bag, name[1])
      table.insert(bag, name[1])
      table.insert(bag, name[1])
      table.insert(bag, name[1])
      table.insert(bag, name[1])
    end
  end

  resetBag()

  function pieces.random()
    -- Refill if all pieces have been drawn
    if #bag == 0 then
      resetBag()
    end

    -- Get a random index for an item in the bag
    local i = math.floor(math.random()*(#bag - 1)) + 1
    -- Retrieve the piece and remove it from the bag
    local name = bag[i]
    table.remove(bag, i)
    return name
  end
end

function pieces.getColor(name)
  local piece = assert(getPiece(name), "invalid name")
  return colors[piece]
end

function pieces.draw(name, dir, x, y, w, h)
  local piece = getPiece(name)
  local c = colors[piece]
  
  local gsx, gsy = w / 5, h / 5
  
  love.graphics.setColor(c[1], c[2], c[3], 1)
  pieces.eachBlock(name, 0, 0, dir, function(gx, gy)
    love.graphics.rectangle("fill", x, y, gsx*gx, gsy*gy)
  end)
end

return pieces

local helium = require("lib.helium")
local useButton = require("lib.helium.shell.button")

return helium(function(param, view)
  return function()
    love.graphics.setColor(0.1, 0.1, 0.1, 1)
    love.graphics.rectangle("fill", 0, 0, view.w, view.h)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("line", 0, 0, view.w, view.h)
  end
end)

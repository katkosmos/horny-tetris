local import = require("src.util.import")
local scriptPath = require("src.util.script_path")

local Cum = {
  GRAVITY = {x=0, y=9.8},
  SHADERS = import("shaders"),
}

function Cum:init()
  local p = scriptPath()..".shaders."
  self.shader = love.graphics.newShader(
    self.SHADERS.pixel,
    self.SHADERS.vertex
  )

  self.particles = {}
  self.walls = {}
  self.mesh = {}
end

function Cum:add(x, y)
  table.insert(self.particles, {p={x=x,y=y},v={x=0,y=0}})
end

function Cum:addWall(x, y, w, h)
  table.insert(self.walls, {p={x=x,y=y},s={x=w,y=h}})
  
  -- Remove cum particles that are inside the newly formed wall
  for i=#self.particles,1,-1 do
    local v = self.particles[i]

    if v.p.x >= x and v.p.x < (x + w) and v.p.y >= y and v.p.y < (y + h) then
        table.remove(self.particles, i)
    end
  end
end

function Cum:removeWallsInside(x,y,width,h)
  for i=#self.walls,1,-1 do
    local w = self.walls[i]

    if x >= w.p.x and x+width < (w.p.x + w.s.x) and y >= w.p.y and y+h < (w.p.y + w.s.y) then
      table.remove(self.walls, i)
    end
  end
end

function Cum:simplifyWalls()
  -- Implement greedy meshing to reduce wall count on X axis
  local rows = {}
  for i,v in ipairs(self.walls) do
    rows[v.p.y] = rows[v.p.y] or {}
    table.insert(rows[v.p.y], v)
  end

  local sort = function(a,b)
    return a.p.x < b.p.x
  end
  
  for _,row in next,rows do
    -- Sort row by X position
    table.sort(row, sort)
    
    -- Search row for walls that can be merged due to their rectangles intersecting
    local toRemove = {}

    local last = row[1]
    for i=2,#row do
      local v = row[i]
      if v.p.x >= last.p.x and v.p.x <= last.p.x + last.s.x then
        table.insert(toRemove, i)
        last.s.x = last.s.x + math.max(0, (v.p.x + v.s.x) - (last.p.x + last.s.x))
      else
        last = v
      end
    end
    
    -- Remove merged walls from row
    table.sort(toRemove)
    for i=#toRemove,1,-1 do
      table.remove(row, toRemove[i])
    end
  end
  
  -- Regenerate walls array (only allocates one table, but probably re-allocates it when growing due to row insertion)
  local walls = {}
  for y,row in next,rows do
    for i,v in ipairs(row) do
      table.insert(walls, v)
    end
  end

  self.walls = walls
end

function Cum:update(dt)
  local needsMeshUpdate = false
  
  local function attraction(p0, p1)
    if p0 == p1 then
      return
    end

    local distSq = (p0.p.x - p1.p.x)^2 + (p0.p.y - p1.p.y)^2
    local dist = math.sqrt(distSq)

    if dist == 0 then
      return
    end
    
    local f = (0.2 / distSq) * dt
    local f2 = (0.0001 / distSq) * dt
    local nx = (p1.p.x - p0.p.x) / dist
    local ny = (p1.p.y - p0.p.y) / dist

    local p0vx = p0.v.x
    p0.v.x = p0vx --[[+ p1.v.x*f]] + nx*f2
    p1.v.x = p1.v.x --[[+ p0vx*f]] - nx*f2
    local p0vy = p0.v.y
    p0.v.y = p0vy --[[+ p1.v.y*f]] + ny*f2
    p1.v.y = p1.v.y --[[+ p0vy*f]] - ny*f2
  end

  for i,v in ipairs(self.particles) do
    -- Apply gravity to velocity
    v.v.x = v.v.x + self.GRAVITY.x*dt
    v.v.y = v.v.y + self.GRAVITY.y*dt
  
    -- Apply velocity to position
    local nx = v.p.x + v.v.x*dt
    local ny = v.p.y + v.v.y*dt
    local nvx, nvy = v.v.x, v.v.y

    for j,vv in ipairs(self.particles) do
      attraction(v, vv)
    end
    
    -- Handle collision
    for j,w in ipairs(self.walls) do
      local tx1 = (w.p.x - v.p.x)/v.v.x
      local tx2 = (w.p.x + w.s.x - v.p.x)/v.v.x
      local ty1 = (w.p.y - v.p.y)/v.v.y
      local ty2 = (w.p.y + w.s.y - v.p.y)/v.v.y

      local tx, ty
      if tx1 >= 0 and tx1 < tx2 then
        tx = tx1
      else
        tx = tx2
      end
      if ty1 >= 0 and ty1 < ty2 then
        ty = ty1
      else
        ty = ty2
      end

      local t = math.min(tx, ty)

      if nx >= w.p.x and nx <= (w.p.x + w.s.x) and ny >= w.p.y and ny <= (w.p.y + w.s.y) then
        if t < dt then
          nx = v.p.x + v.v.x*t
          ny = v.p.y + v.v.y*t
          
          if nx >= w.p.x and nx <= (w.p.x + w.s.x) then
            nvy = -nvy*0.1
          end

          if ny >= w.p.y or ny <= (w.p.y + w.s.y) then
            nvx = -nvx*0.1
          end
        end
      end
    end

    if (not needsMeshUpdate) and (nx ~= v.p.x or ny ~= v.p.y) then
      needsMeshUpdate = true
    end

    v.p.x = nx
    v.p.y = ny
    v.v.x = nvx
    v.v.y = nvy
  end

  if needsMeshUpdate then
    self:updateMesh()
  end
end

function Cum:draw(sx, sy)
  -- Save previous shader and use cum shader
  local shader = love.graphics.getShader()
  love.graphics.setShader(self.shader)

  -- @TODO: draw mesh
  love.graphics.setColor(1, 1, 1, 1)
  for i,v in ipairs(self.walls) do
    --love.graphics.rectangle("line", v.p.x*sx, v.p.y*sy, v.s.x*sx, v.s.y*sy)
  end

  for i,v in ipairs(self.particles) do
    love.graphics.circle("fill", v.p.x*sx, v.p.y*sy, 5)
  end
  
  -- Restore previous shader
  love.graphics.setShader(shader)
end

function Cum:updateMesh()
  -- @TODO: regenerate mesh
end

function Cum:cleanup()
  if self.shader then
    self.shader:release()
    self.shader = nil
  end

  self.particles = nil
  self.walls = nil
  self.mesh = nil
end

return Cum

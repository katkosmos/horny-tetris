return [==[

#pragma language glsl3

vec4 position(mat4 transform_proj, vec4 vertex_pos) {
  return transform_proj * vertex_pos;
}

]==]

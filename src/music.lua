local denver = require("lib.denver")

local function full(freq)
  return {freq = freq, time = 1}
end

local none = {}

local function half(freq)
  return {freq = freq, time = 0.5}
end

local music = {
  notes = {full'B6', none, full'C7', full'A6', none, none, full'B6', none, full'A6', full'A6', full'A6', none, full'G6', none, none, none, full'G#6', none, full'A6', full'F#6', none, none, full'G#6', none, full'F#6', full'F#6', full'F#6', full'E6', none, none},
  compiledNotes = {},
  bpm = 81*2,
  timer = 0,
  nextNote = 1,
}

local tetris = 'RR>AIIA>777ARRIA>>>AIIRRAA7777IIIWnnbWRRRARRIA>>>AIIRRAA7777'

function music:render()
  self.compiledNotes = {}
  local noteCache = {}

  
  for i,note in ipairs(self.notes) do
    if note == none then
      table.insert(self.compiledNotes, {nil, 1 / self.bpm })
    else
      local time = note.time / (self.bpm / 60)
      local n = noteCache[string.format("%s-%f", note.freq, note.time)]
      if not n then
        n = {denver.get({waveform = "square", frequency = note.freq, length = time }), time}
        noteCache[string.format("%s-%f", note.freq, note.time)] = n
      end
      
      table.insert(self.compiledNotes, n)
    end
  end
  
  for i=1,tetris:len() do
    local b = string.byte(tetris, i, i)*4
    local n = self.compiledNotes[((i - 1) % #self.compiledNotes) + 1]
    n[1] = denver.get({waveform = "square", frequency = b, length = n[2]})
  end
end

function music:update(dt)
  self.timer = self.timer - dt

  while self.timer <= 0 do
    local note = self.compiledNotes[self.nextNote]
    if self.nextNote == #self.compiledNotes then
      self.nextNote = 1
    else
      self.nextNote = self.nextNote + 1
    end
    
    if note[1] then
      love.audio.play(note[1])
    end

    self.timer = self.timer + note[2]
  end
end

return music

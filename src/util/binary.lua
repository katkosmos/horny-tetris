local bit = require("bit")

local function binary(binstr)
  local num = 0

  for i=1,string.len(binstr) do
    local b = string.sub(binstr, i, i)
    
    if b == '1' then
      num = bit.bor(bit.lshift(num, 1), 1)
    elseif b == '0' then
      num = bit.lshift(num, 1)
    end
  end

  return num
end

return binary

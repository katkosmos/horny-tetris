local clone
clone = function(tbl)
  if type(tbl) == "table" then
    local newTbl = setmetatable({}, getmetatable(tbl))

    for i,v in next,tbl do
      newTbl[i] = clone(v)
    end

    return newTbl
  else
    return tbl
  end
end

return clone

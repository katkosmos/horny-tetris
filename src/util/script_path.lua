return function(lvl)
   local str = debug.getinfo(lvl or 2, "S").source:sub(2)
   return str:match("(.*/)")
end

local orig_require = require
local script_path = require("src.util.script_path")

local function import(relpath)
  return orig_require(script_path(3)..relpath)
end

return import

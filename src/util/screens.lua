local ScreensMt = {}
local Screens = setmetatable({stack = {}}, ScreensMt)

function ScreensMt:__index(n)
  return function(...)
    local t = ...
    if t == self then
      for _,screen in next,self.stack do
        if type(screen[n]) == "function" then
          screen[n](screen, select(2, ...))
        end
      end
    else
      for _,screen in next,self.stack do
        if type(screen[n]) == "function" then
          screen[n](...)
        end
      end
    end
  end
end

function Screens:push(screen, ...)
  table.insert(self.stack, screen)
  local loadFunc = screen.load or screen.init or screen.setup or screen.start
  if type(loadFunc) == "function" then
    loadFunc(screen, ...)
  end
end

function Screens:pop(screenOrNil)
  if screenOrNil then
    while table.remove(self.stack) ~= screenOrNil do end
  else
    table.remove(self.stack)
  end
end

return Screens

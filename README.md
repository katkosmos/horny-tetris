# Penetris

This game is being made as part of Strawberry Jam 7, a horny game jam. #StrawberryJam

This game is a stack-the-falling-blocks game using pentominoes.

Built with [LÖVE](https://love2d.org/) <3

## Features
 * Pieces fall
 * You can stack them
 * Filling a line clears it

## Why?
Because I can.

## Usage
Don't.

### Actual Usage
Clone the repository and run `love .` in the root of the repository, or [download a pre-built release](https://gitlab.com/katkosmos/penetris/-/releases).
